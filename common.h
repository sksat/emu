#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>

//#define NO_DEBUG

#include "debug.h"

#define KB	1024
#define MB	(1024 * KB)
#define GB	(1024 * MB)

#endif
