TARGET	:= emu
OBJS	:= main.o emulator_base.o emulator.o register_base.o memory.o
OBJS	+= device/device.a
OBJS	+= font/font.o
#OBJS	+= shell/shell.a
#OBJS	+= gui/gui.a
OBJS	+= arch/arch.a

include common.mk

LDFLAGS	+= -pthread -lglfw -lGL

EMU_BIN	:= helloos.img
RUNFLAGS:= --arch x86 --junk-bios --memory-size 1 --fda sample/$(EMU_BIN)

export

default:
	make $(TARGET)

run: $(TARGET) sample/$(EMU_BIN)
	make
	./$(TARGET) $(RUNFLAGS)

clean :
	make -C gui clean
	#make -C shell clean
	make -C font clean
	make -C device clean
	make -C sample clean
	make -C arch clean
	rm -f $(TARGET) $(OBJS)

full :
	make clean
	make

full_run :
	make full
	make  run

$(TARGET) : $(OBJS)
	$(CXX) -o $@ $^ $(LDFLAGS)

sample/$(EMU_BIN):
	make -C sample $(EMU_BIN)

device/device.a:
	make -C device

font/font.o:
	make -C font

shell/shell.a:
	make -C shell

gui/gui.a:
	make -C gui

arch/arch.a:
	make -C arch
